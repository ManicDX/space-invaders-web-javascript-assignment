
//Simple Space Invaders Game

var game;
var rocket;
var dummy;
var barrier;
var winCondition;
var score;

//aliens
var aliens = [];
var alienCount = 0;
var moveRight = false;
var alienMoveUp = 3;
var alienSpeedMultiplier = 1;
var alienWidth 	= 50;
var alienHeight = 50;
var rows = 4;
var cols = 10;
//Alien sprite dimensions
var alSpx = 121;//121 x, 108.5 y for alien sprite sheet
var alSpy = 108;

//Background dimensions
var backWidth = 800;
var backHeight = 600;

//Win lose
var win_lose_w = 400;
var win_lose_h = 200;

var speedCheck = 1;

var collisionTrip = false;

function init() {
    var canvas = document.getElementById("myCanvas");
    game = new Game(canvas);
	
	//Generate aliens
	for(var i = 0; i < rows; i++)
	{
		for(var k = 0; k < cols; k++){
			//Sprite Data
			var j = i % 3;
			var sp = new Sprite(k*50 + 50 + (i*5),i*50 + 50, 25, 25);
			var img = document.getElementById("sprites");
			sp.imageBase(img);
			sp.addImage(0, alSpy * j, alSpx, alSpy);
			sp.addImage(alSpx, alSpy * j, alSpx, alSpy);
			sp.addAnimation("walk",[0, 1]);
			sp.animate("walk");
			sp.setVelocity(-40,0);
			
			//Collision and movement/ win condition check
			sp.collideB = function(r) {
				advanceAliens();
			};
			sp.collide = function() {
				
				alienCount -= 1;
				if(alienCount <= 0)
					winCondition.animate("win");
				game.removeSprite(rocket);
				game.removeSprite(this);
				
				//checkwin(alienCount);
				speedCheck++;

			};
			aliens.push(sp);
			alienCount += 1;
		}
	}
	for(var i = 0; i < aliens.length; i++){
		game.addSprite(aliens[i]);
	}
	
	
	//Screen init
    var back = new Background(0,0,backWidth,backHeight);
    game.setBackground(back);
	
	//Win condition status
    var wl = document.getElementById("winlose");
    winCondition = new Sprite(backWidth/2,backHeight, 200, 200);
    winCondition.imageBase(wl);
	winCondition.addImage(0,0, win_lose_w, win_lose_h);
	winCondition.addImage(0,win_lose_h, win_lose_w, win_lose_h);
	winCondition.addImage(win_lose_w,win_lose_h, win_lose_w, win_lose_h);
	winCondition.addAnimation("win",[0]);
	winCondition.addAnimation("lose",[1]);
	winCondition.addAnimation("play",[2]);
	winCondition.animate("play");
	game.addSprite(winCondition);
	winCondition.setPos(backWidth/2 - 100,backHeight + 50);

	
	//Player and projectile init
    var pimg = document.getElementById("player");
    var player = game.getPlayer();
    sp = new Sprite(backWidth/2,backHeight, 50, 50);
    sp.imageBase(pimg);
    sp.addImage(0,0, alSpx * 2, alSpy);
    player.setSprite(sp);
    player.setPos(backWidth/2,backHeight);
    rocket = new Sprite(0,0,15,25);
    rocket.imageBase(pimg);
    rocket.addImage(0,alSpy,alSpx,alSpy);
    rocket.collideB = function(r) {
        game.removeSprite(rocket);
    };
    player.fire = function() {
		//if(rocket != null)
		//{return;}
        rocket.setPos(player.x+15,player.y-50);
        rocket.setVelocity(0,-110);
        game.addSprite(rocket);
    };
	
	dummy = new Sprite(backWidth/2,backHeight, 50, 50);
    dummy.imageBase(pimg);
    dummy.addImage(alSpx,alSpy, alSpx * 2, alSpy);
	game.addSprite(dummy);
	dummy.setPos(backWidth/2,backHeight);
	dummy.collide = function() {
		game.removeSprite(this);
		winCondition.animate("lose");
	};


	
    game.start(0.1);
	
}

//Move aliens closer, speed them up
function advanceAliens(){
	var tempX;
	for(var i = 0; i < aliens.length; i++){
		
		tempX = aliens[i].dx;
		if(tempX < 0)
			tempX -= alienSpeedMultiplier * speedCheck;
		else
			tempX += alienSpeedMultiplier * speedCheck;
		
		
		aliens[i].setVelocity( 0, alienMoveUp);
		aliens[i].move(1);
		aliens[i].setVelocity(-tempX, 0);
	}
	speedCheck = 1;
}


function stopGame() {
    game.stop();
}

function startGame() {
    game.start(0.1);
}
